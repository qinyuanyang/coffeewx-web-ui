/** When your routing table is too long, you can split it into small modules**/

// 系统管理

import Layout from '@/views/layout/Layout'

const wxmpSystemManage = {
  path: '/wxmpSystemManage',
  component: Layout,
  alwaysShow: true,
  name: '微信管理',
  redirect: 'noredirect',
  meta: {
    title: '微信管理',
    icon: 'list',
    resources: 'wxmpSystemManage'
  },
  children: [
    {
      path: 'accountList',
      component: () => import('@/views/wxmp/system-manage/account-manage/accountList'),
      name: '账号管理',
      meta: { title: '账号管理', icon: 'wechat', noCache: true, resources: 'accountList' }
    },
    {
      path: 'subscribeTextList',
      component: () => import('@/views/wxmp/system-manage/subscribe_text/subscribeTextList'),
      name: '欢迎语管理',
      meta: { title: '欢迎语管理', icon: 'guide', noCache: true, resources: 'subscribeTextList' }
    },
    {
      path: 'receiveTextList',
      component: () => import('@/views/wxmp/system-manage/receive_text/receiveTextList'),
      name: '关键字管理',
      meta: { title: '关键字管理', icon: 'icon-receive-text', noCache: true, resources: 'receiveTextList' }
    },
    {
      path: 'menuList',
      component: () => import('@/views/wxmp/system-manage/menu-manage/menuList'),
      name: '公众号菜单',
      meta: { title: '公众号菜单', icon: 'tree', noCache: true, resources: 'menuList' }
    },
    {
      path: 'accountFansList',
      component: () => import('@/views/wxmp/system-manage/account-fans-manage/accountFansList'),
      name: '粉丝管理',
      meta: { title: '粉丝管理', icon: 'peoples', noCache: true, resources: 'accountFansList' }
    },
    {
      path: 'fansMsgList',
      component: () => import('@/views/wxmp/system-manage/fans-msg-manage/fansMsgList'),
      name: '粉丝消息',
      meta: { title: '粉丝消息', icon: 'message', noCache: true, resources: 'fansMsgList' }
    }
  ]
}

export default wxmpSystemManage
